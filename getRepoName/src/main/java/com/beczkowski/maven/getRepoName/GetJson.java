package com.beczkowski.maven.getRepoName;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.net.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;

public class GetJson {
	
	String getRepoName(){
	JSONParser parser = new JSONParser();
	String repoName = null;
	try {
		String inputL;
		ArrayList<String> repoNames=new ArrayList<String>();
		URL githubURLpushed=new URL("https://api.github.com/users/allegro/repos?sort=pushed");
		URLConnection gp=githubURLpushed.openConnection();
		
		BufferedReader buffer=new BufferedReader(new InputStreamReader(gp.getInputStream()));
		
		
		while ((inputL = buffer.readLine()) != null) {               
			 JSONArray a = (JSONArray) parser.parse(inputL);
        for (Object o : a) {
             JSONObject tutorials = (JSONObject) o;
             String id = (String) tutorials.get("name");
             repoNames.add(id);
		 }
		}
		repoName=repoNames.get(0);
	} catch (MalformedURLException e) {
		e.printStackTrace();
	} catch(IOException e) {
		e.printStackTrace();
	} catch (ParseException e) {
		e.printStackTrace();
	}
	return repoName;
	}
	
	
}


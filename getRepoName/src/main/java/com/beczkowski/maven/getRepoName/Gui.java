package com.beczkowski.maven.getRepoName;

import javax.swing.*;
import java.awt.*;

public class Gui extends JPanel{
	
	private static final long serialVersionUID = 1L;
	
	public Gui(final String updateName){
		setLayout(new GridBagLayout());
        GridBagConstraints ptr = new GridBagConstraints();
        ptr.gridx = 0;
        ptr.gridy = 0;
        ptr.insets = new Insets(5, 5, 5, 5);
        
        add(new JLabel("Latest updated repo Name:"), ptr);
        ptr.gridx++;
        add(new JLabel(updateName), ptr);
	}	
}

package com.beczkowski.maven.getRepoName;


import java.awt.*;

import javax.swing.JFrame;

public class App 
{
    public static void main( String[] args )
    {
    	final GetJson json=new GetJson();
    	EventQueue.invokeLater(new Runnable(){
				public void run(){
			     JFrame guiFrame = new JFrame("GetRepo");
			        guiFrame.setLayout(new BorderLayout());
			        guiFrame.add(new Gui(json.getRepoName()));
			        guiFrame.pack();
			        guiFrame.setLocationRelativeTo(null);
			        guiFrame.setVisible(true);
			        guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
		});
    }
}
